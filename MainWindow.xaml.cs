﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MemRelease
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Fields

        readonly List<ProcessInfo> _processes;
        readonly ListCollectionView _procColView;
        readonly object _procLock;
        Visibility _progVisibility;
        ProcessInfo _selectedProcess;
        bool _canForceGC;

        #endregion

        #region Constructors


        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            _procLock = new object();
            _processes = new List<ProcessInfo>();
            _procColView = new ListCollectionView(_processes);
            _procColView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));

            this.RefreshProcessList();
            this.ProgressVisibility = Visibility.Hidden;
        }

        #endregion

        #region Properties


        public bool CanForceGC
        {
            get { return _canForceGC; }
            private set
            {
                if (_canForceGC != value)
                {
                    _canForceGC = value;
                    this.OnPropertyChanged();
                }
            }
        }


        public ListCollectionView Processes
        {
            get { return _procColView; }
        }


        public Visibility ProgressVisibility
        {
            get { return _progVisibility; }
            private set
            {
                if (_progVisibility != value)
                {
                    _progVisibility = value;
                    this.OnPropertyChanged();
                }
            }
        }


        public ProcessInfo SelectedProcess
        {
            get { return _selectedProcess; }
            set
            {
                if (_selectedProcess != value)
                {
                    _selectedProcess = value;
                    this.OnPropertyChanged();

                    this.CanForceGC = (_selectedProcess != null && _selectedProcess.IsManaged);
                }
            }
        }

        #endregion

        #region Methods


        private void ButtonForceGC_Click(object sender, RoutedEventArgs e)
        {
            Task t = this.ForceGCAsync();
        }


        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            this.RefreshProcessList();
        }


        private Process CreateProcess(string processPath, string args)
        {
            var startInfo = new ProcessStartInfo()
            {
                FileName = processPath,
                Arguments = args,
                CreateNoWindow = true,
                ErrorDialog = true,
                UseShellExecute = false
            };

            var process = new Process() 
            { 
                StartInfo = startInfo,
                EnableRaisingEvents = true
            };
            return process;
        }


        private void DisableWindow()
        {
            this.ProgressVisibility = System.Windows.Visibility.Visible;
            this.IsEnabled = false;
            Mouse.OverrideCursor = Cursors.Wait;
        }


        private void EnableWindow()
        {
            this.ProgressVisibility = System.Windows.Visibility.Hidden;
            this.IsEnabled = true;
            Mouse.OverrideCursor = null;
        }


        private async Task ForceGCAsync()
        {
            try
            {
                this.DisableWindow();
                await Task.Run(() => this.ForceGCImpl());
            }
            catch (Exception ex)
            {
                this.ShowMessageBox("An error occurred while attempting to force a garbage collection: " + ex.Message);
            }
            finally
            {
                this.EnableWindow();
            }
        }


        private void ForceGCImpl()
        {
            string currentPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string heapDumpPath = System.IO.Path.Combine(currentPath, "bin\\HeapDump.exe");
            string args = "/ForceGC " + _selectedProcess.Id.ToString();

            var startInfo = new ProcessStartInfo()
            {
                FileName = heapDumpPath,
                Arguments = args,
                CreateNoWindow = true,
                ErrorDialog = true,
                UseShellExecute = false
            };

            var heapDump = new Process()
            {
                StartInfo = startInfo,
                EnableRaisingEvents = true
            };

            heapDump.Start();
            heapDump.WaitForExit(10000);
            heapDump.Dispose();
        }


        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        private void RefreshProcessList()
        {
            IEnumerable<ProcessInfo> procs = Process.GetProcesses().Select(p => new ProcessInfo(p));
            _processes.Clear();
            _processes.AddRange(procs);
            _procColView.Refresh();
        }


        private void ShowMessageBox(string msg)
        {
            Action action = () => MessageBox.Show(msg, "Memory Release", MessageBoxButton.OK, MessageBoxImage.Information);
            Application.Current.Dispatcher.Invoke(action);
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
