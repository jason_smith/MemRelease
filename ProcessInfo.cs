﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MemRelease
{
    public class ProcessInfo : INotifyPropertyChanged
    {
        #region Fields

        readonly Process _process;
        bool? _isManaged;

        #endregion

        #region Constructors


        public ProcessInfo(Process process)
        {
            _process = process;
        }

        #endregion

        #region Properties


        public int Id
        {
            get { return _process.Id; }
        }


        public bool IsManaged
        {
            get
            {
                if (_isManaged == null)
                {
                    foreach (ProcessModule module in _process.Modules)
                    {
                        if (module.ModuleName.StartsWith("mscor", StringComparison.InvariantCultureIgnoreCase))
                        {
                            _isManaged = true;
                            break;
                        }
                    }

                    if (_isManaged == null)
                        _isManaged = false;
                }

                return _isManaged.Value;
            }
        }


        public long Memory
        {
            get { return _process.PrivateMemorySize64; }
        }


        public string Name
        {
            get { return _process.ProcessName; }
        }

        #endregion

        #region Methods


        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
